# CHANGELOG

## Version 10
* Update schema id to match gnome-extension recommendation
* Add 'about' information into prefs and link to scripts
* Improve disable() function
* Remove unused code for translation
* Set status to error when script execution is returning error

## Version 9
* Update code for Gnome40+ compatibility

## Version 8
* Fix code issue seen after change review

## Version 7
* Cleanup NetExtender script and example
* Improve ip command in NetExtender script
* Cleanup extension ode according to review

## Version 6
* Fix toggle switch state when connection failed

## Version 5
* Add script for `GlobalProtect` from Palo Alto Networks

## Version 4
* Only update indicator when VPN status change
* Embed images into extension
* Check VPN script availability and warn user if not reachable
* Use file chooser to select VPN script in preference
* Copy IP address in clipboard when menu Ip Address is clicked
* Notify that address is available in clipboard
* Allow to disable notification when address is copied in clipboard in preference
* Do not allow check interval bellow 1

## Version 3
* Fix publication error on https://extensions.gnome.org/extension/4061/custom-vpn-toggler/

## Version 2
* Update `shell-version`: Add `3.38`
* Update `Extension web site`: https://gitlab.com/XavierBerger/custom-vpn-toggler

## Version 1
* Initial version.
* Extension Features:
    * VPN start and stop
    * Display VPN IP when VPN is connected
    * Update icon to show if VPN is active or not
    * Configure script to use to interact with VPN
    * Configure VPN check interval
* Documentation:
    * Add repository README.md
    * Add script example for `netExtender`
